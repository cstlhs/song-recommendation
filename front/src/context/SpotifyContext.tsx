import { Playlist } from "@/types";
import axios from "axios";
import { createContext, useContext, useState } from "react";

type ContextProps = {
  playlists: Playlist[];
  fetchPlaylists: () => void;
};

const SpotifyContext = createContext({} as ContextProps);

export const SpotifyProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [playlists, setPlaylists] = useState<Playlist[]>([]);

  const fetchPlaylists = async () => {
    try {
      const { data } = await axios.get("/api/playlists");
      setPlaylists(data.response);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <SpotifyContext.Provider value={{ playlists, fetchPlaylists }}>
      {children}
    </SpotifyContext.Provider>
  );
};

export const useSpotify = () => useContext(SpotifyContext);
