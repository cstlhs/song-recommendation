/* eslint-disable @next/next/no-img-element */

import { gray1 } from "@/support/colors";
import { rem } from "polished";

type Props = {
  name: string;
  image?: string;
  size?: number;
};

export const PlaylistCard = ({ name, image, size }: Props) => {
  return (
    <>
      <div className="PlaylistCard">
        <img className="PlaylistCard--Image" src={image || ""} alt="Foo" />
        <div className="PlaylistCard--Content">
          <div className="PlaylistCard--Content--Title">{name}</div>
          <div className="PlaylistCard--Content--Subtitle">{size} músicas</div>
        </div>
      </div>

      <style jsx>{`
        .PlaylistCard {
          background-color: ${gray1};
          padding: ${rem(12)} ${rem(16)};
          border-radius: ${rem(10)};

          display: flex;
          flex-direction: row;
          align-items: center;
          gap: ${rem(24)};
        }

        .PlaylistCard--Image {
          width: ${rem(72)};
          height: ${rem(72)};
          border-radius: ${rem(10)};
          object-fit: cover;
          object-position: center;
        }

        .PlaylistCard--Content {
          width: calc(100% - ${rem(96)});
          display: flex;
          flex-direction: column;
        }

        .PlaylistCard--Content--Title {
          font-weight: 600;
          font-size: ${rem(20)};
          display: inline-block;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
        }

        .PlaylistCard--Content--Subtitle {
          font-weight: 400;
          font-size: ${rem(12)};
        }
      `}</style>
    </>
  );
};
